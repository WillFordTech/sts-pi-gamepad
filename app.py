import explorerhat as eh
from evdev import InputDevice, categorize, ecodes

gamepad = InputDevice('/dev/input/event0')

print(gamepad)
print ('Starting gamepad read loop')

# Gamepad values are read in a loop in order to continually get the up-to-date
# value of them, allowing us to adjust the motor output accordingly.
for event in gamepad.read.loop():
    if event.type == ecodes.EV_ABS:
        absevent = categorize(event)
        # X Axis (dPad)
        if ecodes.bytype[absevent.event.type][absevent.event.code] == "ABS_HAT0X":
            print('Input ABS_HAT0X with value {val}'.format(val=absevent.event.value))
            if absevent.event.value == -1:
                # Steer left
                eh.motor.one.forward(100)
                eh.motor.two.backward(100)
                print('Turning Left')
            elif absevent.event.value == 0:
                # Stop
                eh.motor.one.stop()
                eh.motor.two.stop()
                print('Stopping')
            elif absevent.event.value == 1:
                # Steer right
                eh.motor.one.backaward(100)
                eh.motor.two.forward(100)
                print('Turning Right')
        # Y Axis (dPad)
        elif ecodes.bytype[absevent.event.type][absevent.event.code] == "ABS_HAT0Y":
            print('Input ABS_HAT0Y with value {val}'.format(val=absevent.event.value))
            if absevent.event.value == -1:
                # Move forward
                eh.motor.one.forward(100)
                eh.motor.two.forward(100)
                print('Moving Forward')
            elif absevent.event.value == 0:
                # Stop
                eh.motor.one.stop()
                eh.motor.two.stop()
                print('Stopping')
            elif absevent.event.value == 1:
                # Move backward
                eh.motor.one.backaward(100)
                eh.motor.two.backaward(100)
                print('Moving Backward')
