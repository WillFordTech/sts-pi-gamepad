STS-Pi Gamepad
==============

This project is a simply Python app to control the STS-Pi robot from Pimoroni using a bluetooth gamepad instead of the web interface the default app uses.  The kit is available here:

https://shop.pimoroni.com/products/sts-pi

This project requires the Pimoroni explorerhat package in order to operate the motors, and also the evdev package in order to read the bluetooth gamepad input. Both are available through pip.

My motors are wired up so the right motor is number one and the left motor is number 2.

The gamepad must be paired with bluetooth to the Raspberry Pi before running the app.  I used an old MadCatz C.T.R.L.r I had laying around as my bluetooth gamepad.

The app can be set to run automatically upon startup of the Raspberry Pi by configuring the default user profile to run it after login (there are plenty of tutorials online for how to do this).